<div class="block">

    <?php if (!empty($errorMessage)) : ?>
        <div class="message errormsg">
            <?php echo $errorMessage; ?>
        </div>
    <?php endif ?>

    <div class="block_head">
        <h2>Create New Blog Post</h2>
    </div>

    <div class="block_content">
        
        <form action="<?php echo site_url('post/index') ?>" method="POST">
            <p>
                <label for="title">
                   Title: <span class="required">*</span>
                </label>

                <input id="title" type="text" name="title" class="text"
                       value= "<?php echo set_value('title') ?>" /><br />
                <span class='note error'>
                    <?php echo form_error('title') ?>
                </span>
            </p>
            <p>
                <label for="description">
                    Body:
                </label>

                <textarea id="description" rows="5" cols="50" name="description"
                          class="wysiwyg"><?php echo set_value('description') ?></textarea> <br/>
                <span class='note error'>
                    <?php echo form_error('description') ?>
                </span>
            </p>
            <p>
                <input type="submit" value="Post" id="submit-event" class="submit small" />
                <input type="button" value="Cancel" id="submit-cancel" class="submit small" />
            </p>
        </form>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->

<link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/time/css/jquery-ui-1.8.14.custom.css') ?>"  />
<script type="text/javascript" src="<?php echo site_url('assets/time/js/jquery.ui.core.min.js') ?>"></script>

