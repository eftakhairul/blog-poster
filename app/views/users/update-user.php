<div class="block">

    <?php if (!empty($errorMessage)) : ?>
    <div class="message errormsg">
        <?php echo $errorMessage ?>
    </div>
    <?php endif ?>

    <div class="block_head">
        <h2>WP Setting</h2>
    </div>

    <div class="block_content">

        <form action="<?php echo site_url('user/index') ?>" method="POST">

            <p>
                <label for="wp_side_address">
                    Wordpress Site: <span class="required">*</span>
                </label>
                <input id="wp_side_address" type="text" name="wp_side_address" class="text"
                       value= "<?php echo $userdata['wp_side_address'] ?>" /> Example: http://your_domain.com
            </p>

            <p>
                <label for="wp_user_name">
                    Wordpress User Name: <span class="required">*</span>
                </label>
                <input id="wp_user_name" type="text" name="wp_user_name" class="text small"
                       value= "<?php echo $userdata['wp_user_name'] ?>" />
            </p>

            <p>
                <label for="wp_password">
                   Wordpress Password: <span class="required">*</span>
                </label>
                <input id="wp_password" type="text" name="wp_password" class="text small"
                       value= "<?php echo  $userdata['wp_password'] ?>" />
            </p>

            <p>
                <input type="submit" value="Update" class="submit small" />
            </p>

        </form>

    </div>		<!-- .block_content ends -->
</div>		<!-- .block ends -->