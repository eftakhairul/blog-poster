<?php
/**
 * Description of User Controller
 *
 * @package     Controller
 * @author      Eftakhairul Islam <eftakhairul@gmail.com>
 */

include_once APPPATH . "controllers/BaseController.php";
class UserController extends BaseController
{
    public function  __construct()
    {
        parent::__construct();
        $this->load->model('users');
    }

    public function index()
    {
        $username = $this->session->userdata('username');

        if (empty($username)) {
            $this->layout->view("post/error");
        } else {
            if (!empty ($_POST)) {
                $userdata = $this->users->find(array("username" => $username));
                $this->users->modify($_POST, $userdata["id"]);
                $this->session->set_flashdata('message', "Wordpress settings are updated.");
                $this->session->set_flashdata('messageType', 'success');
                redirect("/user");
            } else {
                $this->data["userdata"] = $this->users->find(array("username" => $username));
            }

            $this->layout->view('users/update-user', $this->data);
        }
    }

    public function endpoint($user = null)
    {
        if (empty($user)) {
            $this->layout->view("post/error");
        } else {
            $userdata = $this->users->find(array("username" => $user));

            if (!$userdata) {
                $this->layout->view("post/error");

            } else {
                $this->session->set_userdata('username', $userdata['username']);
                $this->session->set_userdata('user_id', $userdata['id']);
                redirect("/post");
            }
        }
    }
}