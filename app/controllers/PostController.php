<?php

/**
 * Description of Group Controller
 *
 * @package     Controller
 * @author      Eftakhairul Islam <eftakhairul@gmail.com>
 * @author      Syed Abidur Rahman <aabid048@gmail.com>
 */

include_once APPPATH . "controllers/BaseController.php";
class PostController extends BaseController
{
    public function  __construct ()
    {
        parent::__construct();
    }


    public function index()
    {
        $username = $this->session->userdata('username');
        if (empty($username)) $this->_redirectForFailure("/user", "Please update your wordpress credentials");

        $this->load->library('form_validation');
        $this->form_validation->setRulesForBlogPost();

        if (!empty($_POST)) {
            if($this->form_validation->run()) {
                include_once("xmlrpc.php");
                $this->load->model("users");
                $userdata = $this->users->find(array("username" => $username));

                $url = $userdata["wp_side_address"] . "/xmlrpc.php";

                 $client = new IXR_Client($url);

                $USER = trim($userdata["wp_user_name"]);
                $PASS = trim($userdata["wp_password"]);


                $content['title']            = $this->input->post("title");
                $content['categories']       = array("Uncategorized");
                $content['description']      = $_POST["description"];
                $content['custom_fields']    = array( array('key' => 'my_custom_fied','value'=>'yes') );
                $content['mt_keywords']      = array('automatic');

                if (!$client->query('metaWeblog.newPost','', $USER,$PASS, $content, true)) {
                    $this->_redirectForFailure("/post", 'Error while creating a new post' . $client->getErrorCode() ." : ". $client->getErrorMessage());

                }

                $ID =  $client->getResponse();
                if($ID)$this->_redirectForSuccess("/post", "Blog post successful. Post ID: " . $ID);

            } else {
                if (empty($username)) $this->_redirectForFailure("/user", "Please insert data correctly.");
            }
        }

        $this->layout->view('post/create', $this->data);
    }
}
