<?php
/**
 * Description of Users
 *
 * @author Eftakhairul Islam <eftakhairul@gmail.com>
 */

class Users extends MY_Model
{
    public function  __construct ()
    {
        parent::__construct();
        $this->loadTable('users', 'id');
    }

    public function modify(array $data, $id)
    {

        if(!empty($data["wp_side_address"])) {
            $str = $data["wp_side_address"];
            $data["wp_side_address"] = substr($str, 0, strlen($str) - +(strrpos($str ,'/',0) == (strlen($str)-1)));
        }
        return $this->update($data, $id);
    }
}